# NCBI SRA Run Suggester

## Description

NCBI SRA Run Suggester is a tool for finding and ordering SRA Run Ids of the organism SARS-CoV-2 from a set
of NCBI SRA "Full XML" xml files. Given a directory of such xml files, the tool will print to standard out the
Run Ids of runs from those files that:

- have the organism descriptor "Severe acute respiratory syndrome coronavirus 2"
- were sequenced with Illumina technology
- have associated read files
- were collected between November 1, 2019 and the current date when the tool is executed

The tool will order the output such that utilizing only the first `n` outputs will minimize size of the largest unsampled 
timespan between the oldest to the newest sample using only `n` samples.

## Example Usage

```
java -jar ncbi-sra-run-suggester.jar /path/to/xml/dir 
```

## Building

From a Maven enabled Bash environment:

``$ ./build.sh``

## Support

This work is supported by the Centers for Disease Control and Prevention (CDC) contract 75D30121C11180.
## License

MIT License

Copyright (c) 2022 Treangen Lab.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.