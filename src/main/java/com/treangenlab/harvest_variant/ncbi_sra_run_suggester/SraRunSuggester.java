package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

interface SraRunSuggester
{
    /**
     * Parses all the given SRA XML files and selects some subset of the contained run ids to send to the provided
     * consumer in an implementation specific way and in an implementation specific order.
     *
     * @param sraExperimentsMetadataXmlFiles the source of run ids and associated metadata. may not be {@code null}.
     * @param runIdConsumer the destination of selected run ids.
     * @throws IOException if there is an unexpected problem accessing the provided files
     * @throws FileParseException if the content format of one of the given files is not expected
     */
    void suggestRunsForIndexing(Iterable<File> sraExperimentsMetadataXmlFiles, Consumer<String> runIdConsumer)
            throws IOException, FileParseException;
}

