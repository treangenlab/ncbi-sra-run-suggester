package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import java.io.File;

/**
 * Thrown to indicate that the contents of a file do not conform to an expected format.
 */
class FileParseException extends Exception
{
    /**
     * Creates a new exception.
     *
     * @param message a description of the error. may be {@code null}.
     * @param file the offending file. may be {@code null}.
     */
    public FileParseException(String message, File file)
    {
        super(message == null ? "bad file" : message +
              " (from file: " + (file == null ? "unknown " : file.getAbsolutePath()) + ")");
    }
}
