package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import java.util.Set;

/**
 * Constructs new {@code SraRunSuggesterTemporalSpacing} instances.
 */
interface SraRunSuggesterTemporalSpacingBuilder
{
    void setMaxSuggestions(long maxSuggestions);

    void setMaxPermissibleSampleBasePairCount(long maxPermissibleSampleBasePairCount);

    void setRunIdsToExclude(Set<String> runIdsToExclude);

    /**
     * Constructs a new {@code SraRunSuggesterTemporalSpacing} instance that is configured with the parameters
     * specified by the most recent invocation of each specifier in this interface. If any given specifier of this
     * interface has not been invoked, the returned suggester will not be constrained by the corresponding specifier's
     * aspect.
     *
     * @return a new suggester instance. never {@code null}.
     */
    SraRunSuggesterTemporalSpacing build();
}
