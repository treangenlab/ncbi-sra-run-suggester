package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

/**
 * Thrown to indicate some designated source did not contain any xml files.
 */
class NoXmlFilesFoundException extends Exception
{
    NoXmlFilesFoundException()
    {
        super("no xml files found");
    }
}
