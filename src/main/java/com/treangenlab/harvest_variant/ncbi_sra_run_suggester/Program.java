package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * The class containing the program entry point.
 */
public class Program
{
    private static final int EXIT_CODE_UNHANDLED_EXCEPTION = 131;

    private static final int EXIT_CODE_HANDLED_MAIN_EXCEPTION = 132;

    private static final int EXIT_CODE_BAD_ARGS_LENGTH = 133;

    private static final int EXIT_CODE_BAD_ARGS_PARSE = 134;

    private static final int EXIT_CODE_BAD_XML_PATH = 135;

    private static final int EXIT_CODE_BAD_SRA_EXCLUDE_PATH = 136;

    private static final int EXIT_CODE_NO_XML_FOUND = 137;

    static
    {
        /*
         * Initialize logging system.
         */
        String log4jPropertyKey = "log4j.configurationFile";
        if(!System.getProperties().containsKey(log4jPropertyKey))
            System.setProperty(log4jPropertyKey, "log4j2.xml"); // unless given a specific config, assume default

        /*
         * Log any unhandled exceptions and terminate if encountered.
         */
        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) ->
        {
            try
            {
                LogManager.getLogger(Program.class).fatal(e.getMessage(), e);
                e.printStackTrace(System.err);
            }
            catch (Throwable ignore)
            {
                // eat un-loggable exceptions
            }

            System.exit(EXIT_CODE_UNHANDLED_EXCEPTION);
        });
    }

    private Program() // Program should never be instantiated because it is just a wrapper for main
    {
    }

    public static void main(String[] args)
    {
        Logger logger = LogManager.getLogger(Program.class);
        logger.debug("entering");

        try
        {
            mainHelp(args);
        }
        catch (IOException | FileParseException e)
        {
            e.printStackTrace(System.err);
            logger.fatal(e.getMessage(), e);
            logger.debug("exiting");
            System.exit(EXIT_CODE_HANDLED_MAIN_EXCEPTION);
        }

        logger.debug("exiting");
    }

    // a version of main that is allowed to throw unrecoverable exceptions
    private static void mainHelp(String[] args) throws IOException, FileParseException
    {
        Logger logger = LogManager.getLogger(Program.class);
        logger.debug("entering");

        /*
         * Define the optional command line arguments to this program.
         */
        Options commandLineOptions = new Options();
        commandLineOptions.addOption("e", "exclude", true, "path to sra id exclusion file");
        commandLineOptions.addOption("m", "max-suggestions", true, "maximum number of sra run ids to suggest");
        commandLineOptions.addOption("bp", "max-base-pairs", true,
                                     "only suggest sra runs ids with samples at or below the indicated number of " +
                                     "base pairs");

        /*
         * Check the count of the command line arguments.
         */
        long numArgsWithValues = commandLineOptions.getOptions().stream().filter(Option::hasArg).count();
        long numFlagArgs       = commandLineOptions.getOptions().stream().filter(o -> !o.hasArg()).count();
        long numMandatoryArgs  = 1;
        if(args.length < numMandatoryArgs || args.length > numMandatoryArgs + numFlagArgs + numArgsWithValues * 2)
        {
            logger.fatal("exiting due to bad command line args count: " + args.length);
            System.out.println("incorrect number of command line arguments: " + args.length);
            System.out.println();
            showUsage(commandLineOptions);
            logger.debug("exiting");
            System.exit(EXIT_CODE_BAD_ARGS_LENGTH);
        }

        /*
         * Parse command line arguments.
         */
        CommandLine commandLineArgs;
        try
        {
            commandLineArgs =  new DefaultParser().parse(commandLineOptions, args);
        }
        catch (ParseException e)
        {
            System.out.println(e.getMessage());
            System.out.println();
            showUsage(commandLineOptions);
            System.exit(EXIT_CODE_BAD_ARGS_PARSE);
            return;
        }

        /*
         * Get mandatory argument indicating where input file or files are.
         */
        File xmlFileOrDirArg = new File(commandLineArgs.getArgs()[0]); // could be a file to parse or a dir of files
        if(!xmlFileOrDirArg.exists())
        {
            logger.fatal("exiting due to xmlFileOrDirArg path not existing: " + xmlFileOrDirArg.getAbsolutePath());
            System.err.println(xmlFileOrDirArg.getAbsolutePath() + " does not exist.");
            System.err.println();
            showUsage(commandLineOptions);
            logger.debug("exiting");
            System.exit(EXIT_CODE_BAD_XML_PATH);
        }

        /*
         * Determine the maximum possible number of suggestions. `null` means unlimited.
         */
        Long maxSuggestionsToGenerate;
        {
            String maxSuggestionsRaw = commandLineArgs.getOptionValue("m", null);
            if (maxSuggestionsRaw == null)
            {
                maxSuggestionsToGenerate = null;
            }
            else
            {
                maxSuggestionsToGenerate = Long.parseLong(maxSuggestionsRaw);
            }
        }

        /*
         * Determine the maximum number of base pairs in any sequence to recommend. `null` means unlimited.
         */
        Long maxSampleBasePairs;
        {
            String maxSampleBasePairsRaw = commandLineArgs.getOptionValue("bp", null);
            if (maxSampleBasePairsRaw == null)
            {
                maxSampleBasePairs = null;
            }
            else
            {
                maxSampleBasePairs = Long.parseLong(maxSampleBasePairsRaw);
            }
        }

        /*
         * If a sra exclude file was specified via command line set it to `sraIdExcludeFile`. Otherwise, set
         * `sraIdExcludeFile` to null.
         */
        File sraIdExcludeFile; // null if not specified by user
        {
            String sraIdExcludeFileRaw = commandLineArgs.getOptionValue("e", null);
            if(sraIdExcludeFileRaw == null)
            {
                sraIdExcludeFile = null;
            }
            else
            {
                sraIdExcludeFile = new File(sraIdExcludeFileRaw);
                if (!sraIdExcludeFile.exists())
                {
                    logger.fatal("sraIdExcludeFile path does not exist: " + sraIdExcludeFile.getAbsolutePath());
                    System.err.println(sraIdExcludeFile.getAbsolutePath() + " does not exist.");
                    System.err.println();
                    showUsage(commandLineOptions);
                    logger.debug("exiting");
                    System.exit(EXIT_CODE_BAD_SRA_EXCLUDE_PATH);
                    throw new RuntimeException("Unreachable because of system exit.");
                }
            }
        }

        /*
         * Determine what sra xml input files define the set of possible suggestions.
         */
        Set<File> sraExperimentsMetadataXmlFiles;
        try
        {
           sraExperimentsMetadataXmlFiles = determineSraXmlFilesFromFileArg(xmlFileOrDirArg.toPath());
        }
        catch (NoXmlFilesFoundException e)
        {
            logger.fatal("exiting due to no xml files found in " + xmlFileOrDirArg.getAbsolutePath());
            System.out.println("No xml files found in " +  xmlFileOrDirArg.getAbsolutePath());
            System.out.println();
            showUsage(commandLineOptions);
            logger.debug("exiting");
            System.exit(EXIT_CODE_NO_XML_FOUND);
            throw new RuntimeException("Unreachable because of system exit.");
        }

        /*
         * Determine if any run ids should be ineligible from being suggested as specified by the user.
         */
        Set<String> runIdsToExclude = sraIdExcludeFile == null ? new HashSet<>() : parseFileToSet(sraIdExcludeFile);

        /*
         * Start generating suggestions.
         */
        SraRunSuggester suggester;
        {
            SraRunSuggesterTemporalSpacingBuilder builder = SraRunSuggesterTemporalSpacing.makeBuilder();
            if (maxSuggestionsToGenerate != null)
                builder.setMaxSuggestions(maxSuggestionsToGenerate);
            if (maxSampleBasePairs != null)
                builder.setMaxPermissibleSampleBasePairCount(maxSampleBasePairs);
            builder.setRunIdsToExclude(runIdsToExclude);
            suggester = builder.build();
        }
        suggester.suggestRunsForIndexing(sraExperimentsMetadataXmlFiles, System.out::println);

        logger.debug("exiting");
    }

    // split file's contents first by lines and then by whitespace to form string set
    private static Set<String> parseFileToSet(File file) throws IOException
    {
        Set<String> fileParts = new HashSet<>();

        /*
         * Split the lines of sraRunIdExclusionFile by whitespace and add each token (assumed to be a sra run id)
         * to the set runIdsToExclude.
         */
        for (String line : Files.readAllLines(file.toPath())) // line at a time in case file is large
            fileParts.addAll(Arrays.asList(line.split("\\s+")));

        return fileParts;
    }

    private static void showUsage(Options commandLineOptions)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java [-Dlog4j.configurationFile=CONFIG.XML] -jar ncbi-sra-run-suggester.jar " +
                             "SRA_METADATA_XML_FILE_OR_XMLS_DIR", commandLineOptions);
    }


    // determine if given path is a file or directory. If a file, return a singleton set of that file. If a directory,
    // return all the ".xml" files in the directory (non-recursive).
    private static Set<File> determineSraXmlFilesFromFileArg(Path xmlFileOrDir) throws NoXmlFilesFoundException,
            IOException
    {
        File[] sraExperimentsMetadataXmlFiles;
        if(xmlFileOrDir.toFile().isDirectory())
        {
            sraExperimentsMetadataXmlFiles = xmlFileOrDir.toFile().listFiles((dir, name) -> name.endsWith(".xml"));

            if(sraExperimentsMetadataXmlFiles == null)
                throw new IOException("Could not read from " + xmlFileOrDir.toFile().getAbsolutePath());

            if(sraExperimentsMetadataXmlFiles.length == 0)
                throw new NoXmlFilesFoundException();
        }
        else if(xmlFileOrDir.toFile().isFile())
        {
            sraExperimentsMetadataXmlFiles = new File[]{xmlFileOrDir.toFile()};
        }
        else
        {
            throw new IOException(xmlFileOrDir.toFile().getAbsolutePath() + " is neither a directory or file");
        }

        return new HashSet<>(Arrays.asList(sraExperimentsMetadataXmlFiles));
    }
}
