package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetNode;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ExperimentPackageSetXmlParser;
import com.treangenlab.harvest_variants.ncbi.sra.experiment_package_set_xml_parser.ParseException;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSample;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSampleSet;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSampleSetBuilder;
import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencingPlatform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SraRunSuggesterTemporalSpacing implements SraRunSuggester
{
    /**
     * Logger for class.
     */
    private final Logger _logger = LogManager.getLogger(SraRunSuggesterTemporalSpacing.class);

    /**
     * The maximum number of suggestions to suggest per invocation of
     * {@link #suggestRunsForIndexing(Iterable, Consumer)}. {@code null} means unlimited.
     */
    private final Long _maxSuggestions;

    /**
     * SRA run ids that should never be suggested.
     */
    private final Set<String> _excludedSraRunIdsLowerCase;

    /**
     * The maximum number of base pairs a sample may have to be eligible for suggestion. {@code null} means unlimited.
     */
    private final Long _maxPermissibleSampleBasePairCount;

    /**
     * Create a suggester that is free to make any number of suggestions without consideration of excluding any run
     * based on its id or number of base pairs within its sample.
     */
    public SraRunSuggesterTemporalSpacing()
    {
        this(Collections.emptySet(), null, null);
    }

    /**
     * Creates as suggester that will suggest up to the given maximum suggestion limit per invocation of
     * {@link #suggestRunsForIndexing} and will also never suggest a run from the given set of run ids or if the run's
     * sample's base pair count is over the given threshold.
     *
     * @param runIdsToExclude run ids that should be excluded from suggestions (case-insensitive). may not be
     *                        {@code null}.
     * @param maxSuggestions the maximum number of suggestions to generate. must be > 0.
     * @param maxPermissibleSampleBasePairCount the maximum number of base pairs a run's sample may contain to be
     *                                          eligible for suggestions. must be > 0.

     */
    public SraRunSuggesterTemporalSpacing(Set<String> runIdsToExclude, long maxSuggestions,
                                          long maxPermissibleSampleBasePairCount)
    {
        this(runIdsToExclude, (Long)maxSuggestions, (Long)maxPermissibleSampleBasePairCount);
    }

    // `null` for `maxSuggestions` or `maxPermissibleSampleBasePairCount` means unlimited maximum
    private SraRunSuggesterTemporalSpacing(Set<String> runIdsToExclude, Long maxSuggestions,
                                           Long maxPermissibleSampleBasePairCount)
    {
        _logger.debug("entering");

        if(maxSuggestions != null && maxSuggestions < 1)
        {
            _logger.warn("maxSuggestions < 1 : " + maxSuggestions);
            _logger.debug("exiting");
            throw new IllegalArgumentException("maxSuggestions must be >=1. Found: " + maxSuggestions);
        }

        if(runIdsToExclude == null)
        {
            _logger.warn("runIdsToExclude null");
            _logger.debug("exiting");
            throw new NullPointerException("runIdsToExclude");
        }

        if(maxPermissibleSampleBasePairCount != null && maxPermissibleSampleBasePairCount < 1)
        {
            _logger.warn("maxPermissibleSampleBasePairCount < 1 : " + maxSuggestions);
            _logger.debug("exiting");
            throw new IllegalArgumentException("maxPermissibleSampleBasePairCount must be >=1. Found: " +
                                                maxPermissibleSampleBasePairCount);
        }

        _maxSuggestions = maxSuggestions;
        _maxPermissibleSampleBasePairCount = maxPermissibleSampleBasePairCount;

        // normalized case-insensitive ids to lower case
        _excludedSraRunIdsLowerCase = new HashSet<>();
        for(String id : runIdsToExclude)
            _excludedSraRunIdsLowerCase.add(id.toLowerCase());

        _logger.debug("exiting");
    }

    /**
     * Selects up to the constructor provided limit number of run ids from the given source after filtering run ids
     * by the constructor provided exclusion set. Suggests ids in an order that aims for the midpoints of contiguous
     * date ranges not yet recommended across the span of the minimum and maximum collection dates of the samples in
     * the input files.
     *
     * @param sraExperimentsMetadataXmlFiles the source of run ids and associated metadata. may not be {@code null}.
     * @param suggestionConsumer the destination for suggestions. may not be {@code null}.
     * @throws IOException if there is an unexpected problem accessing the provided files
     * @throws FileParseException if the content format of one of the given files is not expected
     */
    @Override
    public void suggestRunsForIndexing(Iterable<File> sraExperimentsMetadataXmlFiles,
                                       Consumer<String> suggestionConsumer) throws IOException, FileParseException
    {
        _logger.debug("entering");

        if(sraExperimentsMetadataXmlFiles == null)
        {
            _logger.warn("sraExperimentsMetadataXmlFiles null");
            _logger.debug("exiting");
            throw new NullPointerException("sraExperimentsMetadataXmlFiles");
        }

        if(suggestionConsumer == null)
        {
            _logger.warn("suggestionConsumer null");
            _logger.debug("exiting");
            throw new NullPointerException("suggestionConsumer");
        }

        /*
         * Gather samples described in any of the xml metadata files that are all of:
         *
         * 1. Have associated read files. (Can't call variants if there are no reads! Somehow empties are uploaded.)
         * 2. Are sequenced by Illumina. (Pipeline only works with Illumina at the moment.)
         * 3. Are the organism "Severe acute respiratory syndrome coronavirus 2"
         */
        Collection<SequencedSample> samplesOfInterest =
                gatherSamplesOfInterest(sraExperimentsMetadataXmlFiles);

        suggestRunsForIndexingHelp(samplesOfInterest, suggestionConsumer);

        _logger.debug("exiting");

    }

    // broken out of suggestRunsForIndexing for easier unit testing
    void suggestRunsForIndexingHelp(Collection<SequencedSample> samplesOfInterest, Consumer<String> suggestionConsumer)
    {
        _logger.debug("entering");

        Predicate<SequencedSample> exclusionFilter =
                ((SequencedSample s)-> !_excludedSraRunIdsLowerCase.contains(s.getRunId().toLowerCase()));
        Collection<SequencedSample> filteredSamples =
                samplesOfInterest.stream().filter(exclusionFilter).collect(Collectors.toUnmodifiableList());


        /*
         * Group samples by the day they were collected filtering out samples outside a date range before the pandemic
         * and today. (Outside that range must have incorrect date metadata because those times make no sense).
         */
        Map<LocalDate, List<SequencedSample>> collectionDateToSamplesByGeoTagCountAscending;
        {
            LocalDate assumedDateBeforePandemic = LocalDate.of(2019, 11, 1);

            Map<LocalDate, List<SequencedSample>> collectionDateToSamples
                    = groupSamplesByCollectionDate(filteredSamples, assumedDateBeforePandemic, LocalDate.now());

            /*
             * Sort each sequence sample list by ascending geo-data availability.
             */
            for(List<SequencedSample> samplesOnDay : collectionDateToSamples.values())
                samplesOnDay.sort(Comparator.comparingInt(SequencedSample::getGeoTagsNameCount));

            collectionDateToSamplesByGeoTagCountAscending = collectionDateToSamples; // now sorted
        }

        suggest(collectionDateToSamplesByGeoTagCountAscending, suggestionConsumer);

        _logger.debug("exiting");
    }

    /*
     * Gather samples described in any of the xml metadata files that are all of:
     *
     * 1. Have associated read files. (Can't call variants if there are no reads! Somehow empties are uploaded.)
     * 2. Are sequenced by Illumina. (Pipeline only works with Illumina at the moment.)
     * 3. Are the organism "Severe acute respiratory syndrome coronavirus 2"
     * 4. Have a base pair count threshold below the constructor indicated maximum (if not set to unlimited).
     */
    private Collection<SequencedSample> gatherSamplesOfInterest(Iterable<File> sraExperimentsMetadataXmlFiles)
            throws FileParseException, IOException
    {
        _logger.debug("entering");

        Collection<SequencedSample> sequencedSamplesOfInterestAccum = new LinkedList<>();
        for(File sraExperimentsMetadataXml : sraExperimentsMetadataXmlFiles)
        {
            for (SequencedSample sample : parseFileToSet(sraExperimentsMetadataXml))
            {
                if(sample.getTotalBases().isEmpty()) // skip any sample without an associates read file.
                    continue;

                /*
                 * Skip the sample if a max base pair threshold was specified to the constructor and this sample exceeds
                 * the threshold or the number of base pairs cannot be determined.
                 */
                if(_maxPermissibleSampleBasePairCount != null) // if a threshold is set
                {
                    long totalBases;
                    {
                        String totalBasesAsString = sample.getTotalBases().get();
                        try
                        {
                            totalBases = Long.parseLong(totalBasesAsString);
                        }
                        catch (NumberFormatException e) // can't reason about the number of base pairs
                        {
                            continue;
                        }
                    }

                    if(totalBases > _maxPermissibleSampleBasePairCount)
                        continue;
                }

                if (sample.getPlatform() == SequencingPlatform.Illumina &&
                    sample.getOrganism().orElse("").equals("Severe acute respiratory syndrome coronavirus 2"))
                {
                    sequencedSamplesOfInterestAccum.add(sample);
                }
            }
        }

        _logger.debug("exiting");
        return sequencedSamplesOfInterestAccum;
    }

    private void suggest(Map<LocalDate, List<SequencedSample>> collectionDateToSamplesByGeoTagCountAscending,
                         Consumer<String> suggestionConsumer)
    {
        _logger.debug("entering");

        long numSuggested = 0;

        /*
         * If at least two distinct dates exist from which samples are drawn, recommend a run from the oldest day and
         * the newest day first to demonstrate the breadth of the data chronology.
         */
        if(collectionDateToSamplesByGeoTagCountAscending.size() >= 2)
        {
            LocalDate minCollectionDate = collectionDateToSamplesByGeoTagCountAscending.keySet()
                                                                                       .stream()
                                                                                       .min(LocalDate::compareTo)
                                                                                       .orElseThrow();

            LocalDate maxCollectionDate = collectionDateToSamplesByGeoTagCountAscending.keySet()
                                                                                       .stream()
                                                                                       .max(LocalDate::compareTo)
                                                                                       .orElseThrow();

            String anOldestRunId =
                    removeSampleWithMostGeoDataOnDate(minCollectionDate, collectionDateToSamplesByGeoTagCountAscending);

            String aNewestRunId  =
                    removeSampleWithMostGeoDataOnDate(maxCollectionDate, collectionDateToSamplesByGeoTagCountAscending);

            suggestionConsumer.accept(anOldestRunId);
            if(_maxSuggestions != null && _maxSuggestions == 1)
            {
                _logger.debug("exiting");
                return;
            }

            suggestionConsumer.accept(aNewestRunId);
            numSuggested = 2;
        }

        while(!collectionDateToSamplesByGeoTagCountAscending.isEmpty())
        {
            if(_maxSuggestions != null && numSuggested >= _maxSuggestions)
                break;

            numSuggested = suggestAndRemoveOneRunFromEachDate(collectionDateToSamplesByGeoTagCountAscending,
                                                              suggestionConsumer, numSuggested);
        }

        _logger.debug("exiting");
    }

    private long suggestAndRemoveOneRunFromEachDate(Map<LocalDate, List<SequencedSample>> collectionDateToSamples,
                                                    Consumer<String> suggestionConsumer, long numSuggested)
    {
        _logger.debug("entering");

        if(collectionDateToSamples.isEmpty())
        {
            _logger.warn("collectionDateToSamples empty");
            _logger.debug("exiting");
            throw new IllegalArgumentException("collectionDateToSamples may not be empty");
        }

        /*
         * Create a priority queue of data ranges initialized with a single range that (tightly) spans the collection
         * dates of the given samples.
         */
        PriorityQueue<DateRange> unsampledDateRangesByNumDaysDescending;
        {

            LocalDate minCollectionDate = collectionDateToSamples.keySet().stream()
                                                                          .min(LocalDate::compareTo)
                                                                          .orElseThrow();

            LocalDate maxCollectionDate = collectionDateToSamples.keySet().stream()
                                                                          .max(LocalDate::compareTo)
                                                                          .orElseThrow();

            DateRange collectionDateRange = new DateRange(minCollectionDate, maxCollectionDate);

            unsampledDateRangesByNumDaysDescending = new PriorityQueue<>(Collections.reverseOrder());
            unsampledDateRangesByNumDaysDescending.add(collectionDateRange);
        }

        /*
         * While there still exist date ranges that have not been sampled (or otherwise shown not to contain any samples
         * within the range), take the longest unsampled range and then:
         *
         * 1.) Determine if any samples exist within the date range.  If not, discard the range and try again with next
         *     largest date range.
         * 2.) Find a sample closest to the midpoint of the date range and recommend it.
         * 3.) Split the date range into two smaller date ranges by partitioning the range around the collection date
         *     of the recommended sample. Add those date ranges into the queue.
         */
        while(!unsampledDateRangesByNumDaysDescending.isEmpty())
        {
            if(_maxSuggestions != null && numSuggested >= _maxSuggestions)
                break;

            DateRange largestUnsampledDateRange = unsampledDateRangesByNumDaysDescending.poll();
            LocalDate sampleDate = findClosestDateInRange(largestUnsampledDateRange.Midpoint, largestUnsampledDateRange,
                                                          collectionDateToSamples.keySet());
            if(sampleDate == null) // if no samples within largestUnsampledDateRange, discard range and retry
                continue;

            String runIdOfSampleFromDateWithMostGeoData = removeSampleWithMostGeoDataOnDate(sampleDate,
                                                                                            collectionDateToSamples);

            suggestionConsumer.accept(runIdOfSampleFromDateWithMostGeoData);
            numSuggested++;

            /*
             * Split the largestUnsampledDateRange [L...S...R] into [L...S-1] and [S+1...R] and add each to
             * unsampledDateRangesByNumDaysDescending if the sub-ranges are non-empty.
             *
             * Note that [L...S-1] could actually just be [L] or [S+1...R] could actually just be [R] and that is ok
             * for insertion.
             */
            if(!largestUnsampledDateRange.Start.isEqual(sampleDate))
            {
                DateRange leftRange = new DateRange(largestUnsampledDateRange.Start, sampleDate.minusDays(1));
                unsampledDateRangesByNumDaysDescending.add(leftRange);
            }

            if(!largestUnsampledDateRange.End.isEqual(sampleDate))
            {
                DateRange rightRange = new DateRange(sampleDate.plusDays(1), largestUnsampledDateRange.End);
                unsampledDateRangesByNumDaysDescending.add(rightRange);
            }
        }

        _logger.debug("exiting");
        return numSuggested;
    }

    // find a date closets to target in dates that is still within range, or, null if no such date exists.
    private LocalDate findClosestDateInRange(LocalDate target, DateRange range, Set<LocalDate> dates)
    {
        _logger.debug("entering");

        if(dates.contains(target)) // if dates contains target, return the exact match
        {
            _logger.debug("exiting");
            return target;
        }

        /*
         * Find the date in dates after target that is closest to target and within range (or null if does not exist).
         */
        LocalDate closestAfterTarget = null;
        for(LocalDate i = target.plusDays(1); range.isWithin(i) && closestAfterTarget == null; i = i.plusDays(1))
        {
            if(dates.contains(i))
                closestAfterTarget = i;
        }

        /*
         * Find the date in dates before target that is closest to target and within range (or null if does not exist).
         */
        LocalDate closestBeforeTarget = null;
        for(LocalDate i = target.minusDays(1); range.isWithin(i) && closestBeforeTarget == null; i = i.minusDays(1))
        {
            if(dates.contains(i))
                closestBeforeTarget = i;
        }

        if(closestBeforeTarget == null && closestAfterTarget == null)
        {
            _logger.debug("exiting");
            return null;
        }

        if(closestBeforeTarget == null)
        {
            _logger.debug("exiting");
            return closestAfterTarget;
        }

        if(closestAfterTarget == null)
        {
            _logger.debug("exiting");
            return closestBeforeTarget;
        }

        long deltaBefore = ChronoUnit.DAYS.between(target, closestBeforeTarget);
        long deltaAfter =  ChronoUnit.DAYS.between(target, closestAfterTarget);

        if(deltaBefore < deltaAfter)
        {
            _logger.debug("exiting");
            return closestBeforeTarget;
        }

        _logger.debug("exiting");
        return closestAfterTarget;
    }

    // take all samples in the range [minDate...maxDate] and group them by their collection date.
    private Map<LocalDate, List<SequencedSample>> groupSamplesByCollectionDate(Collection<SequencedSample> samples,
                                                                               LocalDate minDate,
                                                                               LocalDate maxDate)
    {
        _logger.debug("entering");

        Map<LocalDate, List<SequencedSample>> collectionDateToSamples = new HashMap<>();
        for(SequencedSample sample : samples)
        {
            if(sample.getCollectionDate().isEmpty())
                continue;

            LocalDate collectionDate;
            try
            {
                collectionDate = LocalDate.parse(sample.getCollectionDate().get());
            }
            catch (DateTimeParseException e)
            {
                continue;
            }

            if(collectionDate.isBefore(minDate) || collectionDate.isAfter(maxDate))
                continue;

            if(!collectionDateToSamples.containsKey(collectionDate))
                collectionDateToSamples.put(collectionDate, new ArrayList<>());

            collectionDateToSamples.get(collectionDate).add(sample);
        }

        _logger.debug("exiting");
        return collectionDateToSamples;
    }

    private SequencedSampleSet parseFileToSet(File sraExperimentsMetadataXml) throws IOException, FileParseException
    {
        _logger.debug("entering");

        ExperimentPackageSetNode experimentsRoot;
        try(FileInputStream xmlInput = new FileInputStream(sraExperimentsMetadataXml))
        {
            experimentsRoot = ExperimentPackageSetXmlParser.make().parse(xmlInput);
        }
        catch (ParseException e)
        {
            _logger.warn(e.getMessage(), e);
            _logger.debug("exiting");
            throw new FileParseException(e.getMessage(), sraExperimentsMetadataXml);
        }

        SequencedSampleSetBuilder sequencedSamplesBuilder = SequencedSampleSetBuilder.make();
        sequencedSamplesBuilder.addSamplesFromPackagesSkippingInvalid(experimentsRoot);


        SequencedSampleSet result = sequencedSamplesBuilder.build();

        _logger.debug("exiting");
        return result;
    }

    private String removeSampleWithMostGeoDataOnDate(LocalDate date,
                                      Map<LocalDate, List<SequencedSample>> collectionDateToSamplesByGeoDataAscending)
    {
        _logger.debug("entering");

        List<SequencedSample> samplesByGeoDataAscending = collectionDateToSamplesByGeoDataAscending.get(date);
        SequencedSample sampleWithMostGeoData = samplesByGeoDataAscending.remove(samplesByGeoDataAscending.size()-1);

        if(samplesByGeoDataAscending.isEmpty())
            collectionDateToSamplesByGeoDataAscending.remove(date);

        String result = sampleWithMostGeoData.getRunId();

        _logger.debug("exiting");
        return result;
    }

    /**
     * @return a new {@code SraRunSuggesterTemporalSpacingBuilder} instance. never {@code null}.
     */
    static SraRunSuggesterTemporalSpacingBuilder makeBuilder()
    {
        return new SraRunSuggesterTemporalSpacingBuilder()
        {
            private Long _maxSuggestions = null; // null means unlimited

            private Long _maxPermissibleSampleBasePairCount = null; // null means unlimited

            private Set<String> _runIdsToExclude = new HashSet<>();

            @Override
            public void setMaxSuggestions(long maxSuggestions)
            {
                if( maxSuggestions < 1)
                    throw new IllegalArgumentException("maxSuggestions must be >=1. Found: " + maxSuggestions);

                _maxSuggestions = maxSuggestions;
            }

            @Override
            public void setMaxPermissibleSampleBasePairCount(long maxPermissibleSampleBasePairCount)
            {
                if( maxPermissibleSampleBasePairCount < 1)
                {
                    throw new IllegalArgumentException("maxPermissibleSampleBasePairCount must be >=1. Found: " +
                            maxPermissibleSampleBasePairCount);
                }

                _maxPermissibleSampleBasePairCount = maxPermissibleSampleBasePairCount;
            }

            @Override
            public void setRunIdsToExclude(Set<String> runIdsToExclude)
            {
                Objects.requireNonNull(runIdsToExclude);

                _runIdsToExclude = runIdsToExclude;
            }

            @Override
            public SraRunSuggesterTemporalSpacing build()
            {
                return new SraRunSuggesterTemporalSpacing(_runIdsToExclude, _maxSuggestions,
                                                          _maxPermissibleSampleBasePairCount);
            }
        };
    }
}
