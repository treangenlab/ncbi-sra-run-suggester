package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * One or more contiguous calendar dates.
 */
class DateRange implements Comparable<DateRange>
{
    /**
     * Logger for class.
     */
    private final Logger _logger = LogManager.getLogger(DateRange.class);

    /**
     * The earliest date in the range. If this range is a single date, this date will represent the same date
     * as both {@link #Midpoint} and {@link #End}.
     */
    final LocalDate Start;

    /**
     * The date {@code floor((this.DaysCount-1)/2)} days after {@link #Start}.
     */
    final LocalDate Midpoint;

    /**
     * The latest date in the range. If this range is a single date, this date will represent the same date
     * as both {@link #Midpoint} and {@link #Start}.
     */
    final LocalDate End;

    /**
     * The number of days in the range.
     */
    final long DaysCount;

    /**
     * Creates a new range.
     *
     * @param start the earliest date in the date range. may not be {@code null}.
     * @param end the latest date in the date range. may not be {@code null}.
     * @throws IllegalArgumentException if {@code end} occurs before {@code start}
     */
    DateRange(LocalDate start, LocalDate end)
    {
        _logger.debug("entering");

        if(start == null)
        {
            _logger.warn("start is null");
            _logger.debug("exiting");
            throw new NullPointerException("start");
        }

        if(end == null)
        {
            _logger.warn("end is null");
            _logger.debug("exiting");
            throw new NullPointerException("end");
        }

        if(end.isBefore(start))
        {
            _logger.warn("exiting is null");
            _logger.debug("exiting");
            throw new IllegalArgumentException("Ranges do not overlap");
        }

        Start = start;
        End = end;
        DaysCount = ChronoUnit.DAYS.between(start, end)+1;
        Midpoint = start.plusDays((DaysCount-1)/2);

        _logger.debug("exiting");
    }


    // contract from super
    @Override
    public int compareTo(DateRange other)
    {
        _logger.debug("entering");

        if(other == null)
        {
            _logger.warn("other is null");
            _logger.debug("exiting");
            throw new NullPointerException("other");
        }

        int result = Long.compare(DaysCount, other.DaysCount);

        _logger.debug("exiting");
        return result;
    }

    /**
     * Tests if the given date is within this range (exclusive compare with start and end).
     *
     * @param date the date to test. may not be {@code null}
     * @return {@code true} if the date is within this range, otherwise {@code false}.
     */
    boolean isWithin(LocalDate date)
    {
        _logger.debug("entering");

        if(date == null)
        {
            _logger.warn("date is null");
            _logger.debug("exiting");
            throw new NullPointerException("other");
        }

        boolean result = Start.toEpochDay() <= date.toEpochDay() && date.toEpochDay() <= End.toEpochDay();

        _logger.debug("exiting");
        return result;
    }
}