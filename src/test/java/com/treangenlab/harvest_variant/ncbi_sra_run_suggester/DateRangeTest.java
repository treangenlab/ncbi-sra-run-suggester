package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class DateRangeTest
{

    @Test
    public void testConstruction_1Day()
    {
        LocalDate date = LocalDate.of(2001, 1, 1);
        DateRange range = new DateRange(date, date);

        Assert.assertEquals(date, range.Start);
        Assert.assertEquals(date, range.End);
        Assert.assertEquals(date, range.Midpoint);
        Assert.assertEquals(1, range.DaysCount);
    }

    @Test
    public void testConstruction_2Day()
    {
        LocalDate start = LocalDate.of(2001, 1, 1);
        LocalDate end   = LocalDate.of(2001, 1, 2);
        DateRange range = new DateRange(start, end);

        Assert.assertEquals(start, range.Start);
        Assert.assertEquals(end, range.End);
        Assert.assertEquals(LocalDate.of(2001, 1, 1), range.Midpoint);
        Assert.assertEquals(2, range.DaysCount);
    }

    @Test
    public void testConstruction_3Day()
    {
        LocalDate start = LocalDate.of(2001, 1, 1);
        LocalDate end   = LocalDate.of(2001, 1, 3);
        DateRange range = new DateRange(start, end);

        Assert.assertEquals(start, range.Start);
        Assert.assertEquals(end, range.End);
        Assert.assertEquals(LocalDate.of(2001, 1, 2), range.Midpoint);
        Assert.assertEquals(3, range.DaysCount);
    }

    @Test
    public void testConstruction_4Day()
    {
        LocalDate start = LocalDate.of(2001, 1, 1);
        LocalDate end   = LocalDate.of(2001, 1, 4);
        DateRange range = new DateRange(start, end);

        Assert.assertEquals(start, range.Start);
        Assert.assertEquals(end, range.End);
        Assert.assertEquals(LocalDate.of(2001, 1, 2), range.Midpoint);
        Assert.assertEquals(4, range.DaysCount);
    }

    @Test
    public void testConstruction_31Day()
    {
        LocalDate start = LocalDate.of(2001, 1, 1);
        LocalDate end   = LocalDate.of(2001, 1, 31);
        DateRange range = new DateRange(start, end);

        Assert.assertEquals(start, range.Start);
        Assert.assertEquals(end, range.End);
        Assert.assertEquals(LocalDate.of(2001, 1, 16), range.Midpoint);
        Assert.assertEquals(31, range.DaysCount);
    }

    @Test
    public void testConstruction_365Day()
    {
        LocalDate start = LocalDate.of(2001, 1, 1);
        LocalDate end   = LocalDate.of(2001, 12, 31);
        DateRange range = new DateRange(start, end);

        Assert.assertEquals(start, range.Start);
        Assert.assertEquals(end, range.End);
        Assert.assertEquals(LocalDate.of(2001, 7, 2), range.Midpoint);
        Assert.assertEquals(365, range.DaysCount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstruction_InvertedRange()
    {
        LocalDate start = LocalDate.of(2001, 1, 1);
        new DateRange(start, start.minusDays(1));
    }

    @Test
    public void testCompareTo_3v5()
    {
        DateRange three = new DateRange(LocalDate.of(2001, 1, 1), LocalDate.of(2001, 1, 3));
        DateRange five = new DateRange(LocalDate.of(2001, 1, 1),  LocalDate.of(2001, 1, 5));
        Assert.assertEquals(-1, three.compareTo(five));
        Assert.assertEquals(1, five.compareTo(three));
    }

    @Test
    public void testCompareTo_Same()
    {
        DateRange a = new DateRange(LocalDate.of(2001, 1, 1), LocalDate.of(2001, 1, 3));
        DateRange b = new DateRange(LocalDate.of(2001, 1, 1), LocalDate.of(2001, 1, 3));
        Assert.assertEquals(0, a.compareTo(b));
        Assert.assertEquals(0, b.compareTo(a));
    }

    @Test
    public void testIsWithin_Before()
    {
        DateRange range = new DateRange(LocalDate.of(2001, 7, 2), LocalDate.of(2001, 7, 9));
        Assert.assertFalse(range.isWithin(LocalDate.of(2001, 1, 1)));
        Assert.assertFalse(range.isWithin(LocalDate.of(2001, 7, 1)));
    }

    @Test
    public void testIsWithin_After()
    {
        DateRange range = new DateRange(LocalDate.of(2001, 7, 2), LocalDate.of(2001, 7, 9));
        Assert.assertFalse(range.isWithin(LocalDate.of(2001, 12, 1)));
        Assert.assertFalse(range.isWithin(LocalDate.of(2001, 7, 10)));
    }

    @Test
    public void testIsWithin_Within()
    {
        DateRange range = new DateRange(LocalDate.of(2001, 7, 2), LocalDate.of(2001, 7, 9));
        Assert.assertTrue(range.isWithin(LocalDate.of(2001, 7, 2)));
        Assert.assertTrue(range.isWithin(LocalDate.of(2001, 7, 6)));
        Assert.assertTrue(range.isWithin(LocalDate.of(2001, 7, 9)));
    }
}
