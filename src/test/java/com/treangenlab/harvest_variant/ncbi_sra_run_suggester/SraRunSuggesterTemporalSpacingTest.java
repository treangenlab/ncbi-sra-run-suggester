package com.treangenlab.harvest_variant.ncbi_sra_run_suggester;

import com.treangenlab.harvest_variants.ncbi.sra.sequenced_sample.SequencedSample;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SraRunSuggesterTemporalSpacingTest
{
    private SequencedSample makeSample(String date, String id)
    {
        SequencedSample sample = mock(SequencedSample.class);
        when(sample.getCollectionDate()).thenReturn(Optional.of(date));
        when(sample.getRunId()).thenReturn(id);

        return sample;
    }

    @Test
    public void testSuggestRunsForIndexingHelp_2()
    {
        List<SequencedSample> samples = Arrays.asList(makeSample("2020-10-01", "id1"), makeSample("2020-10-10", "id2"));

        List<String> suggestions = new ArrayList<>();
        new SraRunSuggesterTemporalSpacing().suggestRunsForIndexingHelp(samples, suggestions::add);

        Assert.assertEquals(2, suggestions.size());
        Assert.assertEquals("id1", suggestions.get(0));
        Assert.assertEquals("id2", suggestions.get(1));
    }

    @Test
    public void testSuggestRunsForIndexingHelp_3()
    {
        List<SequencedSample> samples = Arrays.asList(makeSample("2020-10-01", "id1"), makeSample("2020-10-10", "id2"),
                                                      makeSample("2020-10-05", "id3"));

        List<String> suggestions = new ArrayList<>();
        new SraRunSuggesterTemporalSpacing().suggestRunsForIndexingHelp(samples, suggestions::add);

        Assert.assertEquals(3, suggestions.size());
        Assert.assertEquals("id1", suggestions.get(0));
        Assert.assertEquals("id2", suggestions.get(1));
        Assert.assertEquals("id3", suggestions.get(2));
    }

    @Test
    public void testSuggestRunsForIndexingHelp_5()
    {
        List<SequencedSample> samples = Arrays.asList(makeSample("2020-10-01", "id1"), makeSample("2020-10-30", "id2"),
                                                      makeSample("2020-10-10", "id3"), makeSample("2020-10-29", "id4"),
                                                      makeSample("2020-10-09", "id5"));

        List<String> suggestions = new ArrayList<>();
        new SraRunSuggesterTemporalSpacing().suggestRunsForIndexingHelp(samples, suggestions::add);

        Assert.assertEquals(5, suggestions.size());
        Assert.assertEquals("id1", suggestions.get(0));
        Assert.assertEquals("id2", suggestions.get(1));
        Assert.assertEquals("id3", suggestions.get(2));
        Assert.assertEquals("id4", suggestions.get(3));
        Assert.assertEquals("id5", suggestions.get(4));
    }

    @Test
    public void testSuggestRunsForIndexingHelp_5_WithExclusion()
    {
        List<SequencedSample> samples = Arrays.asList(makeSample("2020-10-01", "id1"), makeSample("2020-10-30", "id2"),
                                                      makeSample("2020-10-10", "id3"), makeSample("2020-10-29", "id4"),
                                                      makeSample("2020-10-09", "id5"));

        List<String> suggestions = new ArrayList<>();
        SraRunSuggesterTemporalSpacing suggester =
                new SraRunSuggesterTemporalSpacing(Collections.singleton("id5"), Long.MAX_VALUE, Long.MAX_VALUE);
        suggester.suggestRunsForIndexingHelp(samples, suggestions::add);

        Assert.assertEquals(4, suggestions.size());
        Assert.assertEquals("id1", suggestions.get(0));
        Assert.assertEquals("id2", suggestions.get(1));
        Assert.assertEquals("id3", suggestions.get(2));
        Assert.assertEquals("id4", suggestions.get(3));
    }

    @Test
    public void testSuggestRunsForIndexingHelp_5_WithExclusion_Max3()
    {
        List<SequencedSample> samples = Arrays.asList(makeSample("2020-10-01", "id1"), makeSample("2020-10-30", "id2"),
                                                      makeSample("2020-10-10", "id3"), makeSample("2020-10-29", "id4"),
                                                      makeSample("2020-10-09", "id5"));

        List<String> suggestions = new ArrayList<>();
        SraRunSuggesterTemporalSpacing suggester = new SraRunSuggesterTemporalSpacing(Collections.singleton("id5"), 3,
                                                                                      Long.MAX_VALUE);
        suggester.suggestRunsForIndexingHelp(samples, suggestions::add);

        Assert.assertEquals(3, suggestions.size());
        Assert.assertEquals("id1", suggestions.get(0));
        Assert.assertEquals("id2", suggestions.get(1));
        Assert.assertEquals("id3", suggestions.get(2));
    }

    @Test
    public void testSuggestRunsForIndexing_Empty() throws FileParseException, IOException
    {
        Consumer<String> alwaysFail = ((String id) -> Assert.fail());

        new SraRunSuggesterTemporalSpacing().suggestRunsForIndexing(Collections.emptySet(), alwaysFail);
    }
}
